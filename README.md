
### Overview ###

The purpose of this script is to program a Teensy 2.0 board to perform multimedia functionalities found in most keyboards.  Having just purchased a Poker II keyboard that doesn't support play/pause, next/prev tracks on OS X, this was a bit of a let down.  This project is my current solution to this issue.

### Functionality ###

When programmed, this script supports play/pause, next/prev track, and volume adjustments.  All volume adjustments are accomplished through a potentiometer connected to pin F0 on the Teesny board.  Play/pause, previous track, and next track are supported by connecting SPST switches to pins B0, B1, & B2 (respectively).  

Also supported is a volume reset function.  One of the nice things about having buttons for adjusting the volume on a Mac keyboard is that the volume of the system can never 'fall out of sync' with keyboard buttons.  However, with an analog switch, it's relatively easy for the two to fall out of sync simply by connecting a set of headphones to the computer (hence switching to a different volume level the operating system stores).  By holding the previous & next track buttons simultaneously,  the user can crank the volume knob all the way to the right without affecting the system volume.  The knob can then be turned counter-clockwise to lower the system volume and 'sync' the Teensy's volume level with the operating system.

### Compiling & Installing ###

Teensyduino is required load this project to a Teensy 2 board.  Instructions for installing Teensyduino can be found at https://www.pjrc.com/teensy/td_download.html

