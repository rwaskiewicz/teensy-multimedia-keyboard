/*
** Name: Media Buttons
 ** Author: Ryan Waskiewicz 
 ** Purpose: To allow a Teensy2 to emulate the media functionalities of a keyboard.
 ** Note: This project was created with using the Teensy with OSX, and as a result
 ** have volume increments corresponding to OSX's 'fine grain' volume levels
 */

#include <Bounce.h>

#define VOLUMEUPINDEX 3
#define VOLUMEDOWNINDEX 4
#define VOLUME_MAIN_INCREMENTS 16
#define VOLUME_SUB_INCREMENTS 4
#define INCREMENT_DIVISOR (1024 / (VOLUME_MAIN_INCREMENTS * VOLUME_SUB_INCREMENTS))

// Constant pins and debounce times for Bounce objects
const int DEBOUNCE_TIME_MS = 10;
const int BUTTON_COUNT = 3;
const int PLAY_PAUSE_PIN = 0;
const int PREV_TRACK_PIN = 1;
const int NEXT_TRACK_PIN = 2;
const int VOLUME_DIAL_PIN = 21;
const int LED_PIN = 11;

// Array of constant values for media control
const int BUTTONSTOPRESS[5] = {
  KEY_MEDIA_PLAY_PAUSE,
  KEY_MEDIA_PREV_TRACK,
  KEY_MEDIA_NEXT_TRACK,
  KEY_MEDIA_VOLUME_INC,
  KEY_MEDIA_VOLUME_DEC
};

// Create Bounce objects for each press-able button
Bounce playPauseButton = Bounce(PLAY_PAUSE_PIN, DEBOUNCE_TIME_MS);
Bounce prevTrackButton = Bounce(PREV_TRACK_PIN, DEBOUNCE_TIME_MS);
Bounce nextTrackButton = Bounce(NEXT_TRACK_PIN, DEBOUNCE_TIME_MS); 
Bounce * mediaButtons[BUTTON_COUNT] = {
  &playPauseButton, 
  &prevTrackButton, 
  &nextTrackButton
};

int lastVolumeValue = 0;
int lastPotentiometerValue = 0;
boolean resetStateActive = false;
boolean lightStatus = true;

void setup() {
  // Configure the button pins for input mode with pullup resistors.
  pinMode(PLAY_PAUSE_PIN, INPUT_PULLUP);
  pinMode(PREV_TRACK_PIN, INPUT_PULLUP);
  pinMode(NEXT_TRACK_PIN, INPUT_PULLUP);
  pinMode(VOLUME_DIAL_PIN, INPUT_PULLUP);

  // Turn on the LED to signify the Teensy is turned on
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  // Set the internal current volume to the value on the pot
  lastVolumeValue = readVolumePotentiometer();
  lastPotentiometerValue = lastVolumeValue * INCREMENT_DIVISOR;

  delay(500);
}

void loop() {
  checkAndUpdateVolume();
  processButtonStates();
  updateLedStatus();
}

// Check for the falling edge on each of the three buttons,
// act on any reset state or pressed button
void processButtonStates() {
  for (int i = 0; i < BUTTON_COUNT; i++) {
    mediaButtons[i]->update();
  }
  resetStateActive = checkForResetState();
  if (resetStateActive) {
    return; 
  }
  pressButtons();
}  

// Check to see if we have entered/left a reset state
boolean checkForResetState() {
  boolean currentResetState = true;
  for (int i = 1; i < BUTTON_COUNT; i++) {
    currentResetState &= (mediaButtons[i]->read() == 0);
  }
  return currentResetState;
}

// Act on the button(s) that were pressed, provided we are not in a reset state
void pressButtons() {
  for (int i = 0; i < BUTTON_COUNT; i++) {
    if (mediaButtons[i]->fallingEdge()) {
      pressMediaButton(i);
    }
  }
}

// Press a given multimedia button
void pressMediaButton(int index) {
  Keyboard.set_media(BUTTONSTOPRESS[index]);
  Keyboard.send_now();
  Keyboard.set_media(0);
  Keyboard.send_now(); 
} 

void checkAndUpdateVolume() {
  int desiredVolume = readVolumePotentiometer(); 
  if (desiredVolume != lastVolumeValue) {
    int volumeLevelDifference = desiredVolume - lastVolumeValue;
    int volumeStep = volumeLevelDifference/abs(volumeLevelDifference);
    setVolume(desiredVolume, volumeStep);
  }
}

int readVolumePotentiometer() {
  int potentiometerValue = analogRead(VOLUME_DIAL_PIN);
  int desiredVolume = potentiometerValue / INCREMENT_DIVISOR;

  //Protection against flucutations in the value read in causing sporadic volume changes
  if (desiredVolume != lastVolumeValue && abs(potentiometerValue-lastPotentiometerValue) <= 2) {
    return lastVolumeValue;
  }
  else {
    lastPotentiometerValue = potentiometerValue;
    return desiredVolume;
  }
} 

void setVolume(int desiredVolume, int volumeStep) {
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.press(KEY_LEFT_ALT);
  while (desiredVolume != lastVolumeValue) {
    if (volumeStep < 0) {
      pressMediaButton(VOLUMEDOWNINDEX);
    }
    else if (volumeStep > 0 && !resetStateActive) {
      pressMediaButton(VOLUMEUPINDEX);
    }
    lastVolumeValue += volumeStep;
  }
  Keyboard.releaseAll();
}

//Alternate between solid/blinking LED for the reset state
void updateLedStatus() {
  if (resetStateActive || !lightStatus) {
    lightStatus = !lightStatus;
    digitalWrite(LED_PIN, lightStatus);
    delay(100);
  }
}











